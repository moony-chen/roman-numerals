package com.perficient.numero;

public class Numero {
    private int arabicNum;
    private String stringNum;

    public Numero(int arabicNum, String stringNum) {
        this.arabicNum = arabicNum;
        this.stringNum = stringNum;
    }

    public int getArabicNum() {
        return arabicNum;
    }

    public String getStringNum() {
        return stringNum;
    }
}
